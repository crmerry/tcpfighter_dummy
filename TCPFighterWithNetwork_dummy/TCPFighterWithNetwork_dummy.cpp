/* ***************************************

**************************************** */
#include "stdafx.h"
#include "Contents.h"
#include "Protocol.h"
#include "Log.h"
#include "RingBuffer.h"
#include "Session.h"
#include "Character.h"
#include "NetworkProcedure.h"

/* ***************************************
lib
**************************************** */
#pragma comment(lib, "Winmm.lib")
#pragma comment(lib, "Ws2_32.lib")
/* ***************************************
매크로
**************************************** */

/* ***************************************
함수
**************************************** */
/* ***************************************
초기화 & 정리
**************************************** */
bool InitializeDummy();
bool InitializeEnvrionment();
bool InitializeNetwork();

void ReleaseDummy();
void ReleaseEnvrionment();
void ReleaseNetwork();

void TerminateDummy();

/* ***************************************
네트워크.
**************************************** */
void NetworkMainProc();
void DistributeSessionsToSelect();
void CallSelect(Session** sessions, FD_SET* r_set, FD_SET* w_set, size_t count);
bool Connect();

/* ***************************************
더미 관련 직접적 
**************************************** */
void DummyUpdate();
void DummyAction(Character* const character);
bool DeadReckoning(short* out_estimate_x, short* out_estimate_y, Character* const character);
bool ComparePosition(short pos_x1, short pos_y1, short pos_x2, short pos_y2);
void PrintStatus();
void KeyControl();

/* ***************************************
전역 변수.
**************************************** */
bool g_b_run;
wchar_t g_server_IP[16];
int g_session_count_max;
int g_session_count;
int g_loop_count;
int g_send_count;
int g_recv_count;
unsigned int g_RTT_accumulated;
unsigned int g_RTT_max;
unsigned int g_RTT_count;
bool g_b_echo;
std::map <SOCKET, Session*> g_session_map;
std::map <int, Character*> g_character_map;

/* ***************************************
브로드캐스트용 전역변수.
except_list에는 각 패킷의 사이즈와 제외될 ID가 들어가 있음.
**************************************** */
std::list<BroadCastUnit> g_broadcast_except_list;
RingBuffer g_broadcast_Q;

int main(void)
{
	if (!InitializeDummy())
	{
		return 0;
	}

	g_b_run = true;

	while (g_b_run)
	{
		
		NetworkMainProc();

		DummyUpdate();

		PrintStatus();
		KeyControl();
		g_loop_count++;
	}

	ReleaseDummy();

}

bool InitializeDummy()
{
	if (!InitializeLog())
	{
		assert(0 && L"Can not initialize LOG");

		return false;
	}

	if (!InitializeEnvrionment())
	{
		assert(0 && L"Can not Initialize envrionment.");
		LOG(LOG_LEVEL_ERROR, L"Can not Initialize envrionment.");

		return false;
	}

	if (!InitializeNetwork())
	{
		assert(0 && L"Can not Initialize network.");
		LOG(LOG_LEVEL_ERROR, L"Can not Initialize network.");

		return false;
	}

	return true;
}

bool InitializeEnvrionment()
{
	timeBeginPeriod(1);
	_wsetlocale(LC_ALL, L"korean");

	wprintf(L"Server IP : ");
	wscanf_s(L"%s", g_server_IP, (unsigned int)_countof(g_server_IP));
	wprintf(L"Connect Count : ");
	scanf_s("%d", &g_session_count_max);

	return true;
}

bool InitializeNetwork()
{
	int error;
	WSADATA wsa;

	error = WSAStartup(MAKEWORD(2, 2), &wsa);

	if( error != 0)
	{
		assert(0 && L"Can not Initialize WSAStartup");
		LOG(LOG_LEVEL_ERROR, L"Can not Initialize WSAStartup.");
		
		return false;
	}

	return Connect();
}

void ReleaseDummy()
{
	ReleaseNetwork();
	ReleaseEnvrionment();
}

void ReleaseEnvrionment()
{
	timeEndPeriod(1);
}

void ReleaseNetwork()
{
	WSACleanup();
}

void TerminateDummy()
{
	g_b_run = false;
}

/* ****************************************
네트워크 메인
**************************************** */
void NetworkMainProc()
{
	DistributeSessionsToSelect();
}

/* ***************************************
select는 FD_SETSIZE에 의하여 64개의 소켓만
조사가 가능하다. FD_SETSIZE를 늘려주면
한 번에 select할 수 있는 양을 늘려줄 수 있지만,
검색 자체에 너무 많은 시간을 투자하게 되어
반응성이 느려질 수 있다.
따라서 select을 나누어서 해줌.
**************************************** */
void DistributeSessionsToSelect()
{
	/* ***************************************
	세션 최대 숫자 만큼을 배열로 설정.
	select을 나누어서 호출할 예정.
	**************************************** */
	Session* sessions[SESSION_COUNT_MAX + 1];

	/* ***************************************
	select을 나누어서 호출하듯, 브로드 캐스트 역시
	나누어서 해줌. brodcast_packet_size는 브로드 캐스트 버퍼에
	있는 패킷의 사이즈... 중간에 셀렉트 콜에 의하여 브로드 캐스트 버퍼에
	패킷이 추가되더라도 변하지 않도록, 현재 단계에서 설정.
	**************************************** */
	int broadcast_packet_size = 0;

	if (g_broadcast_except_list.size() > 0)
	{
		broadcast_packet_size = g_broadcast_except_list.front()._packet_size;
	}

	auto session_iter = g_session_map.begin();
	auto session_end = g_session_map.end();

	size_t session_size = g_session_map.size();
	size_t index = 0;

	/* ***************************************
	세션 리스트에서 세션들을 배열에 대입.
	**************************************** */
	while (session_iter != session_end)
	{
		sessions[index] = session_iter->second;

		++session_iter;
		++index;
	}

	/* ***************************************
	fd_set_size 만큼 나누어서 select를 호출.
	**************************************** */
	const size_t fd_set_size = SELECT_SIZE;
	size_t loop_max = session_size / fd_set_size;
	size_t loop_left = session_size % fd_set_size;

	FD_SET r_set;
	FD_SET w_set;

	size_t sessions_index = 0;

	for (unsigned int index = 0; index < loop_max; index++)
	{
		sessions_index = index*fd_set_size;

		if (broadcast_packet_size)
		{
			SendProcBroadcastPacketByDistribute(&sessions[sessions_index], fd_set_size, broadcast_packet_size, false);
		}

		FD_ZERO(&r_set);
		FD_ZERO(&w_set);

		for (unsigned int count = 0; count < fd_set_size; count++)
		{
			if (sessions[count + sessions_index])
			{
				FD_SET(sessions[count + sessions_index]->_socket, &r_set);

				if (sessions[count + sessions_index]->_send_Q->GetUsedSize())
				{
					FD_SET(sessions[count + sessions_index]->_socket, &w_set);
				}
			}
		}

		CallSelect(&sessions[sessions_index], &r_set, &w_set, fd_set_size);
	}

	/* ***************************************
	나머지 호출
	**************************************** */
	/* ***************************************
	호출할 세션이 없으면 종료
	**************************************** */
	if (loop_left == 0) 
	{
		return;
	}

	FD_ZERO(&r_set);
	FD_ZERO(&w_set);

	sessions_index = loop_max*fd_set_size;

	if (broadcast_packet_size)
	{
		/* ***************************************
		마지막 브로드 캐스트 이므로, 패킷을 보내준 후
		마지막 인자를 true로 하여, 보낸 만큼 땡겨주도록함.
		**************************************** */
		SendProcBroadcastPacketByDistribute(&sessions[sessions_index], loop_left, broadcast_packet_size, true);
	}

	for (unsigned int index = 0; index < loop_left; index++)
	{
		if (sessions[index + sessions_index])
		{
			FD_SET(sessions[index + sessions_index]->_socket, &r_set);

			if (sessions[index + sessions_index]->_send_Q->GetUsedSize())
			{
				FD_SET(sessions[index + sessions_index]->_socket, &w_set);
			}
		}
	}

	/* ***************************************
	일반 적으로, loop_left가 0인 경우(즉, r_set과 w_set에 들어간 소켓이 없는 경우)에 CallSelect를 호출하면, selec함수가 에러를 리턴한다.
	때문에, loop_left가 0인 경우를 걸러줘야 한다. 그러나, 위에서 listen 소켓을 매번 넣으므로, r_set에 listen socket이 계속 들어가게 되어,
	문제 없이 진행 가능.
	**************************************** */
	CallSelect(&sessions[sessions_index], &r_set, &w_set, loop_left);
}

void CallSelect(Session** sessions, FD_SET* r_set, FD_SET* w_set, size_t count)
{
	timeval time{ 0,0 };

	int ret_val = select(0, r_set, w_set, NULL, &time);

	if (ret_val > 0)
	{
		for (unsigned int index = 0; index < count; index++)
		{
			if (FD_ISSET(sessions[index]->_socket, r_set))
			{
				RecvProc(sessions[index]);
			}

			if (FD_ISSET(sessions[index]->_socket, w_set))
			{
				SendProc(sessions[index]);

				/* ***************************************
				보내고 끊기를 위한 설정.
				**************************************** */
				if (sessions[index]->_b_to_be_disconnect)
				{
					DisconnectSession(sessions[index]);

					continue;
				}
			}

			/* ***************************************
			바로 끊어 준다.
			**************************************** */
			if (sessions[index]->_b_right_disconnect)
			{
				DisconnectSession(sessions[index]);
			}
		}
	}
	else if (ret_val == SOCKET_ERROR)
	{
		int error = WSAGetLastError();

		LOG(LOG_LEVEL_ERROR, L"select error. error code : %d, select_size : %I64u ", error, count);

		TerminateDummy();
	}
}

bool Connect()
{
	SOCKADDR_IN server_address;
	ZeroMemory(&server_address, sizeof server_address);

	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(SERVER_PORT);
	int error = InetPton(AF_INET, g_server_IP, &server_address.sin_addr);

	/* ***************************************
	error 값이 1이면 정상, 0 이면 잘못된 IP 주소, -1인 경우 에러 코드 확인.
	**************************************** */
	if (error == 0)
	{
		LOG(LOG_LEVEL_ERROR, L"IP is wrong");

		return false;
	}
	else if (error == -1)
	{
		error = WSAGetLastError();
		LOG(LOG_LEVEL_ERROR, L"InetPton error. code : %d ", error);

		return false;
	}

	while (g_session_count < g_session_count_max)
	{
		SOCKET client_socket = socket(AF_INET, SOCK_STREAM, 0);

		if (client_socket == INVALID_SOCKET)
		{
			error = WSAGetLastError();

			wprintf(L"Connect failed\n");
			LOG(LOG_LEVEL_ERROR, L"Connect failed. INVALID_SOCKET : %d", error);

			continue;
		}

		error = connect(client_socket, (SOCKADDR*)&server_address, sizeof server_address);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			wprintf(L"Connect failed\n");
			LOG(LOG_LEVEL_ERROR, L"Connect failed. SOCKET_ERROR : %d", error);

			continue;
		}

		unsigned long nonblocking_on = 1;
		error = ioctlsocket(client_socket, FIONBIO, &nonblocking_on);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			wprintf(L"Connect failed\n");
			LOG(LOG_LEVEL_ERROR, L"Can not change block socket to non block socket : %d", error);

			continue;
		}

		bool nodelay_on = true;
		error = setsockopt(client_socket, IPPROTO_TCP, TCP_NODELAY, (char*)&nodelay_on, sizeof nodelay_on);

		if (error == SOCKET_ERROR)
		{
			error = WSAGetLastError();

			wprintf(L"Connect failed\n");
			LOG(LOG_LEVEL_ERROR, L"Can not change TCP_NODELAY : %d", error);

			continue;
		}

		Session* session = nullptr;

		if (!CreateSession(&session, client_socket, server_address))
		{
			wprintf(L"Connect failed\n");
			LOG(LOG_LEVEL_ERROR, L"Connect failed. Can not create session");
		}

		g_session_count++;
		wprintf(L"Connect Success : %d\n", g_session_count);
	}

	wprintf(L"Connect Try : %d, Success : %d", g_session_count_max, g_session_count);

	return true;
}

void DummyUpdate()
{
	auto iter = g_character_map.begin();
	auto end = g_character_map.end();

	unsigned int current_time = timeGetTime();

	int count = 0;
	
	while (iter != end && count < 20)
	{
		if (iter->second->_next_action_time < current_time)
		{
			DummyAction(iter->second);
			
			if (!g_b_echo)
			{
				NetEchoCS(*(iter->second));
				g_b_echo = true;
			}

			count++;
		}

		++iter;
	}

	Sleep(1);
}

void DummyAction(Character* const character)
{	
	short estimate_x;
	short estimate_y;

	short old_x = character->_pos_x;
	short old_y = character->_pos_y;

	DeadReckoning(&estimate_x, &estimate_y, character);

	short move_x = abs(estimate_x - old_x);
	short move_y = abs(estimate_y - old_y);

	SetCharacterPosition(character, estimate_x, estimate_y);
	SetCharacterActionPosition(character, estimate_x, estimate_y);

	unsigned char action = rand() % 12;
	unsigned char prev_action = character->_action;

	if (action >= 8 && action <= 10)
	{
		action += 3;
	}
	else if (action == 11)
	{
		action = ACTION_STAND;
	}

	if (character->_action_x <= 50)
	{
		action = ACTION_MOVE_RR;
	}

	if (character->_action_x >= 6350)
	{
		action = ACTION_MOVE_LL;
	}

	if (character->_action_y <= 50)
	{
		action = ACTION_MOVE_DD;
	}

	if (character->_action_y >= 6350)
	{
		action = ACTION_MOVE_UU;
	}

	SetCharacterAction(character, action);
	SetCharacterDirection(character, action);

	LOG(LOG_LEVEL_DEBUG, L"ID : %d, action : %d, pos : (%d, %d), socket : %I64u", character->_ID, action, character->_pos_x, character->_pos_y, character->_session->_socket);

	switch (action)
	{
	case ACTION_MOVE_LL:
	case ACTION_MOVE_LU:
	case ACTION_MOVE_UU:
	case ACTION_MOVE_RU:
	case ACTION_MOVE_RR:
	case ACTION_MOVE_RD:
	case ACTION_MOVE_DD:
	case ACTION_MOVE_LD:
		NetMoveStartCS(*character);
		break;

	case ACTION_ATTACK1:
		if (prev_action >= 0 && prev_action <= 7)
		{
			NetMoveStopCS(*character);
		}

		NetAttack1CS(*character);
		break;

	case ACTION_ATTACK2:
		if (prev_action >= 0 && prev_action <= 7)
		{
			NetMoveStopCS(*character);
		}

		NetAttack2CS(*character);
		break;

	case ACTION_ATTACK3:
		if (prev_action >= 0 && prev_action <= 7)
		{
			NetMoveStopCS(*character);
		}

		NetAttack3CS(*character);
		break;

	case ACTION_STAND:
		NetMoveStopCS(*character);
		break;

	default:
		break;
	}

	character->_last_action_time = timeGetTime();
	character->_next_action_time = character->_last_action_time + 3000 + rand() % 500;
}

bool DeadReckoning(short* out_estimate_x, short* out_estimate_y, Character* const character)
{
	/* ***************************************
	액션을 시작했을 때의 위치 + 움직임 액션 + 방향 + 지난 시간을 통하여
	캐릭터의 위치를 추측.
	**************************************** */
	unsigned char action = character->_action;

	short last_action_x = character->_action_x;
	short last_action_y = character->_action_y;

	short move_distance_x = MOVE_DISTANCE_X;
	short move_distance_y = MOVE_DISTANCE_Y;

	short estimate_x = 0;
	short estimate_y = 0;

	/* ***************************************
	시간 간격을 통하여 몇 프레임인지 계산.
	**************************************** */
	//int dif_time = character->_session->_current_packet_time - character->_session->_last_packet_time;
	int dif_time = timeGetTime() - character->_last_action_time;

	if (dif_time < SECOND_PER_FRAME)
	{
		*out_estimate_x = last_action_x;
		*out_estimate_y = last_action_y;

		return false;
	}

	int multiply = (dif_time / SECOND_PER_FRAME);

	/* ***************************************
	이동 거리는 프레임 * 프레임당 이동 거리.
	**************************************** */
	move_distance_x *= multiply;
	move_distance_y *= multiply;

	/* ***************************************
	액션에 따라, 추정된 위치를 구해준다.
	**************************************** */
	switch (action)
	{
	case ACTION_MOVE_LL:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			estimate_x = MAP_MOVE_LEFT + 1;
		}

		break;

	case ACTION_MOVE_LU:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y - move_distance_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_LEFT + 1;
			estimate_y = max(last_action_y - dif_y2, MAP_MOVE_TOP + 1);
		}
		else if (estimate_y <= MAP_MOVE_TOP)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = max(MAP_MOVE_LEFT + 1, last_action_x - dif_x2);
			estimate_y = MAP_MOVE_TOP + 1;
		}

		break;

	case ACTION_MOVE_LD:
		estimate_x = last_action_x - move_distance_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_x <= MAP_MOVE_LEFT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_LEFT + 1;
			estimate_y = min(last_action_y + dif_y2, MAP_MOVE_BOTTOM - 1);
		}
		else if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = MAP_MOVE_BOTTOM - last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = max(MAP_MOVE_LEFT + 1, last_action_x - dif_x2);
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}

		break;

	case ACTION_MOVE_RR:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			estimate_x = MAP_MOVE_RIGHT - 1;
		}

		break;

	case ACTION_MOVE_RU:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y - move_distance_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = MAP_MOVE_RIGHT - last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_RIGHT - 1;
			estimate_y = max(last_action_y - dif_y2, MAP_MOVE_TOP + 1);
		}
		else if (estimate_y <= MAP_MOVE_TOP)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = min(MAP_MOVE_RIGHT - 1, last_action_x + dif_x2);
			estimate_y = MAP_MOVE_TOP + 1;
		}

		break;

	case ACTION_MOVE_RD:
		estimate_x = last_action_x + move_distance_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_x >= MAP_MOVE_RIGHT)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_x2 = MAP_MOVE_RIGHT - last_action_x;
			short dif_y2 = (dif_x2 * dif_y1) / dif_x1;

			estimate_x = MAP_MOVE_RIGHT - 1;
			estimate_y = min(last_action_y + dif_y2, MAP_MOVE_BOTTOM - 1);
		}
		else if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			short dif_x1 = move_distance_x;
			short dif_y1 = move_distance_y;

			short dif_y2 = MAP_MOVE_BOTTOM - last_action_y;
			short dif_x2 = (dif_y2 * dif_x1) / dif_y1;

			estimate_x = min(MAP_MOVE_RIGHT - 1, last_action_x + dif_x2);
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}

		break;

	case ACTION_MOVE_UU:
		estimate_x = last_action_x;
		estimate_y = last_action_y - move_distance_y;

		if (estimate_y <= MAP_MOVE_TOP)
		{
			estimate_y = MAP_MOVE_TOP + 1;
		}

		break;

	case ACTION_MOVE_DD:
		estimate_x = last_action_x;
		estimate_y = last_action_y + move_distance_y;

		if (estimate_y >= MAP_MOVE_BOTTOM)
		{
			estimate_y = MAP_MOVE_BOTTOM - 1;
		}
		break;

	case ACTION_STAND:
	case ACTION_ATTACK1:
	case ACTION_ATTACK2:
	case ACTION_ATTACK3:
		estimate_x = last_action_x;
		estimate_y = last_action_y;
		break;

	default:
		LOG(LOG_LEVEL_ERROR, L"Wrong Action type. type : %d, socket : %I64u, ID : %d", action, character->_session->_socket, character->_ID);
		MakeSessionDisconnectThisTime(character->_session);
		break;
	}

	/* ***************************************
	out of range 체크
	**************************************** */
	estimate_x = max(MAP_MOVE_LEFT + 1, estimate_x);
	estimate_x = min(MAP_MOVE_RIGHT - 1, estimate_x);
	estimate_y = max(MAP_MOVE_TOP + 1, estimate_y);
	estimate_y = min(MAP_MOVE_BOTTOM - 1, estimate_y);

	*out_estimate_x = estimate_x;
	*out_estimate_y = estimate_y;

	/* ***************************************
	추정치와 실제 위치를 비교한 값을 리턴.
	**************************************** */
	return ComparePosition(character->_pos_x, character->_pos_y, estimate_x, estimate_y);
}

bool ComparePosition(short pos_x1, short pos_y1, short pos_x2, short pos_y2)
{
	int dif_x = abs(pos_x1 - pos_x2);
	int dif_y = abs(pos_y1 - pos_y2);

	return (dif_x < MOVE_LIMIT_DISTANCE && dif_y < MOVE_LIMIT_DISTANCE);
}

void PrintStatus()
{
	static unsigned int start = timeGetTime();
	unsigned int end = timeGetTime();

	if (end - start < 1000)
	{
		return;
	}

	start = end;

	struct tm t;
	time_t current;

	time(&current);
	localtime_s(&t, &current);

	wchar_t time_string[64];
	swprintf_s(time_string, 64, L"[%d_%d_%d_%d_%d_%d]", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec);

	wprintf(L"\n");
	wprintf(L"%s Loop/sec : %d \n", time_string, g_loop_count);
	wprintf(L"%s Packet/sec : %d \n", time_string, g_send_count + g_recv_count);
	wprintf(L"%s Recv/sec : %d \n", time_string, g_recv_count);
	wprintf(L"%s Send/sec : %d \n", time_string, g_send_count);
	if (g_RTT_count != 0)
	{
		wprintf(L"%s RTT : Aver : %dms / Max : %dms \n", time_string, g_RTT_accumulated / g_RTT_count, g_RTT_max);
	}
	else
	{
		wprintf(L"%s RTT : Not checked \n", time_string);
	}

	wprintf(L"%s try : %d / success : %d / fail : %d \n", time_string, g_session_count_max, g_session_count, g_session_count_max - g_session_count);

	LOG(LOG_LEVEL_SYSTEM, L"Loop / sec : %d", g_loop_count);
	LOG(LOG_LEVEL_SYSTEM, L"Packet/sec : %d", g_send_count + g_recv_count);
	LOG(LOG_LEVEL_SYSTEM, L"Recv/sec : %d",  g_recv_count);
	LOG(LOG_LEVEL_SYSTEM, L"Send/sec : %d",  g_send_count);
	
	if (g_RTT_count != 0)
	{
		LOG(LOG_LEVEL_SYSTEM, L"RTT : Aver : %dms / Max : %dms",  g_RTT_accumulated / g_RTT_count, g_RTT_max);
	}
	else
	{
		LOG(LOG_LEVEL_SYSTEM, L"RTT : Not checked");
	}
	
	LOG(LOG_LEVEL_SYSTEM, L"try : %d / success : %d / fail : %d \n", g_session_count_max, g_session_count, g_session_count_max - g_session_count);

	g_loop_count = 0;
	g_send_count = 0;
	g_recv_count = 0;
	g_RTT_accumulated = 0;
	g_RTT_count = 0;
	g_RTT_max = 0;
}

void KeyControl()
{
	static bool b_control_onoff = false;
	static unsigned int last_call = GetTickCount();
	unsigned int current_call = GetTickCount();

	do
	{
		if (current_call - last_call < 1000)
		{
			break;
		}

		if (GetAsyncKeyState(VK_F3) & 0x8001 && GetAsyncKeyState(0x55) & 0x8001) // 0x55 : u
		{
			b_control_onoff = true;
		}

		if (GetAsyncKeyState(VK_F4) & 0x8001 && GetAsyncKeyState(0x4c) & 0x8001) // 0x4c :L
		{
			b_control_onoff = false;
		}

		/* ***************************************
		일부 연결 끊기.
		**************************************** */
		if (GetAsyncKeyState(0x57) & 0x8001 && b_control_onoff) // 0x57 : w
		{
			int max = rand() % 50;

			auto iter = g_session_map.begin();
			auto end = g_session_map.end();

			int count = 0;

			while (iter != end && count < max)
			{
				Session* session = iter->second;
				++iter;
				++count;
				DisconnectSession(session);
			}
		}

		/* ***************************************
		일부 연결 하기.
		**************************************** */
		if (GetAsyncKeyState(0x45) & 0x8001 && b_control_onoff) // 0x45 : e
		{
			Connect();
		}

		/* ***************************************
		전부 연결 끊기
		**************************************** */
		if (GetAsyncKeyState(0x51) & 0x8001 && b_control_onoff) // 0x51 : q
		{
			TerminateDummy();
		}

		last_call = current_call;

	} while (0);
}
