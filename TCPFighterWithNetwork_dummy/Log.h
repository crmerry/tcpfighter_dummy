#pragma once

/* ***************************************
로그용 전역 변수 Log.cpp
**************************************** */
extern int g_log_level;
extern wchar_t g_log_temp_buf[1024];

/* ***************************************
로그용 함수
**************************************** */
bool InitializeLog();
void ReleaseLog();
void Log(wchar_t* log_string, int log_level);

/* ***************************************
로그용 매크로
**************************************** */
#define WIDE2(x) L##x
#define WIDE1(x) WIDE2(x)
#define WFILE WIDE1(__FILE__)

#define LOG_LEVEL_DEBUG		(0)
#define LOG_LEVEL_WARNING	(1)
#define LOG_LEVEL_ERROR		(2)
#define LOG_LEVEL_SYSTEM	(3)

#define LOG(log_level, format, ...)									\
do {																\
	if (g_log_level <= log_level)									\
	{																\
		swprintf_s(g_log_temp_buf, 1024, format, ##__VA_ARGS__);	\
		Log(g_log_temp_buf, log_level);								\
	}																\
} while(0)															
