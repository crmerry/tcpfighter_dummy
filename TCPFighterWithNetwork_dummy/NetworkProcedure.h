#pragma once

struct Session;
struct TargetSector;
class SerializationBuffer;

/* ***************************************
구조체
**************************************** */
struct BroadCastUnit
{
	int	_except_ID;
	int	_packet_size;
};

/* ***************************************
패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type);
void CreatePacketMoveStartCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack1CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack2CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketAttack3CS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketMoveStopCS(SerializationBuffer * const packet, unsigned char direction, unsigned short x, unsigned short y);
void CreatePacketEchoCS(SerializationBuffer* const packet, unsigned int time);

/* ***************************************
네트워크 프로시져
**************************************** */
void NetCreateMyCharacterSC(SerializationBuffer* const packet, Session* const session);
void NetCreateOtherCharacterSC(SerializationBuffer* const packet, Session* const session);
void NetMoveStartSC(SerializationBuffer* const packet, Session* const session);
void NetMoveStopSC(SerializationBuffer* const packet, Session* const session);
void NetAttack1SC(SerializationBuffer* const packet, Session* const session);
void NetAttack2SC(SerializationBuffer* const packet, Session* const session);
void NetAttack3SC(SerializationBuffer* const packet, Session* const session);
void NetDamageSC(SerializationBuffer* const packet, Session* const session);
void NetDeleteSC(SerializationBuffer* const packet, Session* const session);
void NetEchoSC(SerializationBuffer* const packet, Session* const session);
void NetSyncSC(SerializationBuffer* const packet, Session* const session);

void NetMoveStartCS(const Character& character);
void NetAttack1CS(const Character& character);
void NetAttack2CS(const Character& character);
void NetAttack3CS(const Character& character);
void NetMoveStopCS(const Character& character);
void NetEchoCS(const Character& character);

/* ***************************************
네트워크 수신부
**************************************** */
void RecvProc(Session* const session);
bool RecvPacket(Session* const session);
bool DecodePacket(unsigned short* const type, SerializationBuffer* const packet, Session* const session);
void ProcPacket(unsigned short type, SerializationBuffer* const packet, Session* const session);

/* ***************************************
네트워크 송신부
**************************************** */
bool SendProc(Session* const session);
void SendPacketByUnicast(SerializationBuffer* const packet, Session* const session);
void SendPacketByBroadcast(SerializationBuffer* const packet, int except_ID);
void SendProcBroadcastPacketByDistribute(Session** session, size_t size, int packet_size, bool b_next);