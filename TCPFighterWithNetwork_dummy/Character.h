#pragma once

struct Session;

/* ***************************************
구조체
**************************************** */
struct Character
{
	int				_ID;
	Session*		_session;

	unsigned char	_action;
	unsigned char	_direction;

	short			_pos_x;
	short			_pos_y;
	short			_action_x;
	short			_action_y;

	short			_sector_x;
	short			_sector_y;

	char			_HP;

	unsigned int	_last_action_time;
	unsigned int	_next_action_time;
};

/* ***************************************
함수
**************************************** */
void InitializeCharacter(Character* const character, Session* const session);

bool CreateCharacter(Character** out_character, Session* const session);
void DeleteCharacter(Character* const character);

bool AddCharacter(Character* const character);
bool RemoveCharacter(Character* const character);

bool FindCharacter(Character** Character, int ID);

void SetCharacterPosition(Character* const character, short x, short y);
void SetCharacterActionPosition(Character* const character, short x, short y);
bool IsCharacterMove(Character* const character);
void SetCharacterDirection(Character* const character, unsigned char direction);
void SetCharacterID(Character* const character, int ID);
void SetCharacterHP(Character* const character, unsigned char HP);
void SetCharacterAction(Character* const character, unsigned char action);
