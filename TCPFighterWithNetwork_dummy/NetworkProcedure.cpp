#include "stdafx.h"
#include "Protocol.h"
#include "Contents.h"
#include "Log.h"
#include "RingBuffer.h"
#include "SerializationBuffer.h"
#include "Character.h"
#include "NetworkProcedure.h"
#include "Session.h"

extern std::list<BroadCastUnit> g_broadcast_except_list;
extern RingBuffer g_broadcast_Q;

extern std::map <SOCKET, Session*> g_session_map;
extern std::map <int, Character*> g_character_map;

extern int g_send_count;
extern int g_recv_count;
extern unsigned int g_RTT_accumulated;
extern unsigned int g_RTT_max;
extern unsigned int g_RTT_count;
extern bool g_b_echo;

extern void TerminateDummy();

/* ***************************************
패킷 생성
**************************************** */
void CreatePacketHeader(SerializationBuffer* const packet, unsigned char code, unsigned char size, unsigned char type)
{
	*packet << code;
	*packet << size;
	*packet << type;
	*packet << (unsigned char)0; // temp 쓰지 않느다.
}

/* ***************************************
무브에서의 방향은, 캐릭터의 액션(이동)을 보내줘야함.
**************************************** */
void CreatePacketMoveStartCS(SerializationBuffer* const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack1CS(SerializationBuffer * const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack2CS(SerializationBuffer * const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketAttack3CS(SerializationBuffer * const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketMoveStopCS(SerializationBuffer * const packet, unsigned char direction, unsigned short x, unsigned short y)
{
	*packet << direction;
	*packet << x;
	*packet << y;
	*packet << (unsigned char)PACKET_END_CODE;
}

void CreatePacketEchoCS(SerializationBuffer* const packet, unsigned int time)
{
	*packet << time;
	*packet << (unsigned char)PACKET_END_CODE;
}
/* ***************************************
네트워크 프로시져
**************************************** */
void NetCreateMyCharacterSC(SerializationBuffer* const packet, Session* const session)
{
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;
	unsigned char HP;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;
	(*packet) >> HP;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_CREATE_MY_CHARACTER_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d, HP : %d, socket : %I64u", ID, direction, pos_x, pos_y, HP, session->_socket);

	SetSessionID(session, ID);

	Character* character = nullptr;

	if (CreateCharacter(&character, session))
	{
		SetCharacterPosition(character, pos_x, pos_y);
		SetCharacterActionPosition(character, pos_x, pos_y);
		SetCharacterDirection(character, direction);
		SetCharacterHP(character, HP);
	}
	else
	{
		LOG(LOG_LEVEL_ERROR, L"Can not create character. socket : %I64u, ID : %d, session_map_size : %I64u, character_map_size : %I64u", session->_socket, ID, g_session_map.size(), g_character_map.size());
	}
}

void NetCreateOtherCharacterSC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시.
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;
	unsigned char HP;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;
	(*packet) >> HP;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_CREATE_OTHER_CHARACTER_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d, HP : %d", ID, direction, pos_x, pos_y, HP);
}

void NetMoveStartSC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_MOVE_START_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d", ID, direction, pos_x, pos_y);
}

void NetMoveStopSC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_MOVE_STOP_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d", ID, direction, pos_x, pos_y);
}

void NetAttack1SC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_ATTACK1_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d", ID, direction, pos_x, pos_y);
}

void NetAttack2SC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_ATTACK2_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d", ID, direction, pos_x, pos_y);
}

void NetAttack3SC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int ID;
	unsigned char direction;
	unsigned short pos_x;
	unsigned short pos_y;

	(*packet) >> ID;
	(*packet) >> direction;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_ATTACK3_SC : ID : %d, direction : %d, pos_x : %d, pos_y : %d", ID, direction, pos_x, pos_y);
}

void NetDamageSC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	무시
	**************************************** */
	int attack_ID;
	int damage_ID;
	unsigned char damage_HP;
	
	(*packet) >> attack_ID;
	(*packet) >> damage_ID;
	(*packet) >> damage_HP;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_DAMAGE_SC : attack_ID : %d, damage_ID : %d, damage_HP : %d", attack_ID, damage_ID, damage_HP);
}

void NetDeleteSC(SerializationBuffer* const packet, Session* const session)
{
	/* ***************************************
	더미이므로, 세션 아이디와 패킷의 ID가 같은 경우만 처리.
	**************************************** */

	int ID;

	(*packet) >> ID;

	LOG(LOG_LEVEL_DEBUG, L"Packet : PACKET_DELETE_CHARACTER_SC : ID : %d", ID);

	if (session->_ID != ID)
	{
		return;
	}

	Character* character = nullptr;

	if (!FindCharacter(&character, ID))
	{
		LOG(LOG_LEVEL_ERROR, L"Can not find character. target_ID : %d, session_ID : %d, session_map_size : %I64u, character_map_size : %I64u", ID, session->_ID, g_session_map.size(), g_character_map.size());
	}

	MakeSessionDisconnectThisTime(session);
}

void NetEchoSC(SerializationBuffer* const packet, Session* const session)
{
	unsigned int time;

	*packet >> time;

	unsigned int dif_time = timeGetTime() - time;
	g_RTT_accumulated += dif_time;
	g_RTT_count++;
	g_RTT_max = max(g_RTT_max, dif_time);
	g_b_echo = false;
}

void NetSyncSC(SerializationBuffer* const packet, Session* const session)
{
	int ID;
	short pos_x;
	short pos_y;

	(*packet) >> ID;
	(*packet) >> pos_x;
	(*packet) >> pos_y;

	Character* character = nullptr;

	FindCharacter(&character, ID);

	if (session->_ID == ID && character->_ID && ID)
	{
		LOG(LOG_LEVEL_WARNING, L"Packet : PACKET_SC_SYNC : ID : %d, pos : (%d, %d)", ID, pos_x, pos_y);
		wprintf(L"SyncPacket. ID : %d, pos : (%d, %d)\n", ID, pos_x, pos_y);

		SetCharacterActionPosition(character, pos_x, pos_y);
	}
}

void NetMoveStartCS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 5, PACKET_MOVE_START_CS);
	CreatePacketMoveStartCS(&packet, character._action, character._pos_x, character._pos_y);

	SendPacketByUnicast(&packet, character._session);
}

void NetAttack1CS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK1_CS);
	CreatePacketAttack1CS(&packet, character._direction, character._pos_x, character._pos_y);

	SendPacketByUnicast(&packet, character._session);
}

void NetAttack2CS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK2_CS);
	CreatePacketAttack2CS(&packet, character._direction, character._pos_x, character._pos_y);

	SendPacketByUnicast(&packet, character._session);
}

void NetAttack3CS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 5, PACKET_ATTACK3_CS);
	CreatePacketAttack3CS(&packet, character._direction, character._pos_x, character._pos_y);

	SendPacketByUnicast(&packet, character._session);
}

void NetMoveStopCS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 5, PACKET_MOVE_STOP_CS);
	CreatePacketMoveStopCS(&packet, character._direction, character._pos_x, character._pos_y);

	SendPacketByUnicast(&packet, character._session);
}

void NetEchoCS(const Character& character)
{
	SerializationBuffer packet;

	CreatePacketHeader(&packet, PACKET_HEADER_CODE, 4, PACKET_ECHO_CS);
	CreatePacketEchoCS(&packet, timeGetTime());

	SendPacketByUnicast(&packet, character._session);
}

/* ***************************************
네트워크 수신부
**************************************** */
void RecvProc(Session* const session)
{
	SerializationBuffer packet;
	unsigned short type = PACKET_NOT_USED;

	bool b_recv = RecvPacket(session);

	while (b_recv)
	{
		if (!DecodePacket(&type, &packet, session))
		{
			break;
		}

		ProcPacket(type, &packet, session);

		packet.Clear();
	}
}

bool RecvPacket(Session* const session)
{
	int read_count = recv(session->_socket, session->_recv_Q->GetWriteBufferPtr(), session->_recv_Q->GetWritableSizeAtOnce(), 0);

	if (read_count > 0)
	{
		session->_recv_Q->MoveRearAfterWrite(read_count);

		return true;
	}
	else if (read_count == 0)
	{
		/* ***************************************
		FIN 이나 RST가 온것.
		즉 상대방에서 더 이상 보낼 것이 없다는 표시에
		의하여, 내 소켓도 recv에 0을 반환.
		**************************************** */
		MakeSessionDisconnectThisTime(session);

		return false;
	}
	else
	{
		int error = WSAGetLastError();

		/* ***************************************
		논블럭 소켓이므로 우드블럭 확인.
		**************************************** */
		if (error == WSAEWOULDBLOCK)
		{
			return false;
		}

		/* ***************************************
		접속 끊고 로그
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"recv error. error_code : %d, socket : %I64u, session_ID : %d", error, session->_socket, session->_ID);

		MakeSessionDisconnectThisTime(session);

		return false;
	}
}

bool DecodePacket(unsigned short* const type, SerializationBuffer* const packet, Session* const session)
{
	PacketHeader header;
	RingBuffer* recv_Q = session->_recv_Q;

	if (recv_Q->GetUsedSize() < sizeof(PacketHeader))
	{
		return false;
	}

	recv_Q->Peek((char*)&header, sizeof(PacketHeader));
	*type = header._type;

	if (header._code != PACKET_HEADER_CODE)
	{
		/* ***************************************
		패킷 코드가 다르다
		1 : 악성 패킷
		2 : 클라 혹은 서버 패킷 코드 문제
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Packet code error. socket : %I64u, session_ID : %d, packet_code : %d", session->_socket, session->_ID, header._code);

		MakeSessionDisconnectThisTime(session);

		return false;
	}

	int total_packet_size = sizeof(PacketHeader) + header._size + PACKET_END_CODE_SIZE;

	if (recv_Q->GetUsedSize() < total_packet_size)
	{
		return false;
	}

	recv_Q->MoveFrontAfterRead(sizeof(PacketHeader));

	recv_Q->Dequeue(packet->GetBufferPtr(), header._size);
	recv_Q->MoveFrontAfterRead(PACKET_END_CODE_SIZE);
	packet->MoveWritePos(header._size);

	/* ***************************************
	디코드 패킷에 성공해야, 패킷이 제대로 온것이므로
	패킷 받은 시간을 디코드 패킷에서 설정.
	**************************************** */
	//session->_current_packet_time = timeGetTime();

	return true;
}

void ProcPacket(unsigned short type, SerializationBuffer* const packet, Session* const session)
{
	g_recv_count++;

	switch (type)
	{
	case PACKET_CREATE_MY_CHARACTER_SC:
		NetCreateMyCharacterSC(packet, session);
		break;
	
	case PACKET_CREATE_OTHER_CHARACTER_SC:
		NetCreateOtherCharacterSC(packet, session);
		break;

	case PACKET_MOVE_START_SC:
		NetMoveStartSC(packet, session);
		break;
	
	case PACKET_MOVE_STOP_SC:
		NetMoveStopSC(packet, session);
		break;

	case PACKET_ATTACK1_SC:
		NetAttack1SC(packet, session);
		break;

	case PACKET_ATTACK2_SC:
		NetAttack2SC(packet, session);
		break;

	case PACKET_ATTACK3_SC:
		NetAttack3SC(packet, session);
		break;

	case PACKET_DELETE_CHARACTER_SC:
		NetDeleteSC(packet, session);
		break;

	case PACKET_DAMAGE_SC:
		NetDamageSC(packet, session);
		break;

	case PACKET_ECHO_SC:
		NetEchoSC(packet, session);
		break;

	case PACKET_SYNC_SC:
		NetSyncSC(packet, session);
		break;

	default:
		/* ***************************************
		서버에서 패킷이 잘못 온 것
		or
		클라에서 패킷을 잘못 해석한것.
		접속종료처리 + 로그 처리
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Packet type error. socket : %I64u, session_ID : %d, packet_type : %d", session->_socket, session->_ID, type);

		MakeSessionDisconnectThisTime(session);

		break;
	}
}

/* ***************************************
네트워크 송신부
**************************************** */
bool SendProc(Session* const session)
{
	RingBuffer* send_Q = session->_send_Q;
	g_send_count++;

	while (1)
	{
		if ((send_Q->GetUsedSize()) == 0)
		{
			return false;
		}

		int send_size = send(session->_socket, send_Q->GetReadBufferPtr(), send_Q->GetReadableSizeAtOnce(), 0);

		if (send_size == SOCKET_ERROR)
		{
			int error = WSAGetLastError();

			/* ***************************************
			논블럭 소켓이므로 우드블럭 체크
			**************************************** */
			if (error == WSAEWOULDBLOCK)
			{
				return true;
			}

			/* ***************************************
			접속 끊고 로그
			**************************************** */
			LOG(LOG_LEVEL_ERROR, L"Send proc error. error_coode : %d, socket : %I64u, session_ID : %d, ", error, session->_socket, session->_ID);

			MakeSessionDisconnectThisTime(session);

			return false;
		}

		send_Q->MoveFrontAfterRead(send_size);
	}
}

void SendPacketByUnicast(SerializationBuffer* const packet, Session* const session)
{
	int data_size = packet->GetDataSize();

	int enqueue_size = session->_send_Q->Enqueue(packet->GetBufferPtr(), data_size);

	if (enqueue_size != data_size)
	{
		/* ***************************************
		센드 큐에 들어가지 못했다는 것은.
		수신 측이 제대로 수신->처리를 못하고 있다는 것.
		혹은 서버에서 너무 많은 패킷을 보내고 있음.
		**************************************** */
		LOG(LOG_LEVEL_ERROR, L"Can not enqueue data into send_Q. size : %d", session->_send_Q->GetBufferSize());

		MakeSessionDisconnectThisTime(session);
	}
}

/* ***************************************
브로드캐스트용 버퍼에 넣어준다. except_ID가 0이면 사용 x 1이상이면 해당 ID에 해당하는 세션은
제외됨.
**************************************** */
void SendPacketByBroadcast(SerializationBuffer* const packet, int except_ID)
{
	int data_size = packet->GetDataSize();

	int enqueue_size = g_broadcast_Q.Enqueue((char*)packet->GetBufferPtr(), data_size);
	g_broadcast_except_list.push_back(BroadCastUnit{ except_ID, data_size });

	if (enqueue_size != data_size)
	{
		// 브로드 캐스트용 링버퍼가 터진 상황.
		// 혹은 링버퍼 논리 오류.
		LOG(LOG_LEVEL_ERROR, L"Broadcast Q explosion. size : %d", g_broadcast_Q.GetBufferSize());

		TerminateDummy();
	}
}

/* ***************************************
브로드 캐스트용 버퍼에 패킷이 있는 경우,
이 패킷을 다시 각 세션의 송신 버퍼에 넣어줌.

이 함수는 DistributeSessionsToSelect()안에서
브로드 캐스트 -> 셀렉트 콜의 순서가 반복적으로 이루어짐.
이 때, 셀렉트 콜의 결과로 브로드 캐스트용 큐에 다시 패킷이 추가될 수 있음.
이 때, 추가된 만큼 다시 보내는 것이 아니고,
DistributeSessionsToSelect() 함수 호출 초기에 브로드 캐스트용 버퍼에 있는 패킷 크기(packet_size)를 조사하여, 그 사이즈만큼만 보내준다.
그리고 DistributeSessionsToSelect()콜이 끝날 때,  packet_size만큼 땡겨준다.
그 결과, 셀렉트 콜 중에 생긴 브로드 캐스트용 패킷은 브로드 캐트스 버퍼에 남아있고,
다음 DistributeSessionsToSelect()에서 다시 보내주면 됨.
**************************************** */
void SendProcBroadcastPacketByDistribute(Session** session, size_t size, int packet_size, bool b_next)
{
	int readalbe_size_at_once = g_broadcast_Q.GetReadableSizeAtOnce();
	int except_ID = g_broadcast_except_list.front()._except_ID;
	
	g_send_count++;

	if (readalbe_size_at_once >= packet_size)
	{
		for (size_t index = 0; index < size; index++)
		{
			if (session[index]->_ID == except_ID)
			{
				continue;
			}

			int enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetReadBufferPtr(), packet_size);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != packet_size)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				MakeSessionDisconnectThisTime(session[index]);
			}
		}
	}
	else
	{
		/* ***************************************
		브로드 캐스트용 버퍼에서 한 번에 읽어올 수 있는 사이즈가 패킷보다 작아서
		한 번에 enqueue를 할 수 없는 상황임. 따라서 두 번에 걸쳐서 패킷을 세션의 송신버퍼에 삽입해야함.
		**************************************** */
		for (size_t index = 0; index < size; index++)
		{
			if (session[index]->_ID == except_ID)
			{
				continue;
			}

			/* ***************************************
			첫 번째 삽입.
			**************************************** */
			int enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetReadBufferPtr(), readalbe_size_at_once);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != readalbe_size_at_once)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				DisconnectSession(session[index]);
			}

			/* ***************************************
			두 번째 삽입을 위한 여분 계산
			**************************************** */
			int left_size = packet_size - readalbe_size_at_once;

			/* ***************************************
			두 번째 삽입일 때는, 버퍼의 마지막에서 버퍼의 처음으로
			돌아온 것을 기준으로 계산된 것.
			**************************************** */
			enqueue_size = session[index]->_send_Q->Enqueue(g_broadcast_Q.GetBufferPtr(), left_size);

			/* ***************************************
			세션의 센드 큐가 터진 상황. 끊어줌.
			**************************************** */
			if (enqueue_size != left_size)
			{
				LOG(LOG_LEVEL_WARNING, L"Disconnect session by broadcast. socket : %I64u session_ID : %d size of packets : %d ", session[index]->_socket, session[index]->_ID, packet_size);

				DisconnectSession(session[index]);
			}
		}
	}

	/* ***************************************
	모든 세션에 패킷을 보내줬으므로,
	보낸만큼 땡겨줌.
	**************************************** */
	if (b_next)
	{
		g_broadcast_Q.MoveFrontAfterRead(packet_size);
		g_broadcast_except_list.pop_front();
	}
}